-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Sep 2021 pada 05.59
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `industridzaco`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_config`
--

CREATE TABLE `tbl_config` (
  `idconfig` int(11) NOT NULL,
  `pin` varchar(6) DEFAULT NULL,
  `waktudata` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_config`
--

INSERT INTO `tbl_config` (`idconfig`, `pin`, `waktudata`) VALUES
(1, '567567', '2021-09-05 19:55:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_interview`
--

CREATE TABLE `tbl_interview` (
  `idinterview` int(11) NOT NULL,
  `idpelamar` char(12) DEFAULT NULL,
  `p1` text DEFAULT NULL,
  `p2` text DEFAULT NULL,
  `p3` text DEFAULT NULL,
  `p4` text DEFAULT NULL,
  `p5` text DEFAULT NULL,
  `p6` text DEFAULT NULL,
  `p7` text DEFAULT NULL,
  `p8` text DEFAULT NULL,
  `p9` text DEFAULT NULL,
  `p10` text DEFAULT NULL,
  `p11` text DEFAULT NULL,
  `waktudata` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_interview`
--

INSERT INTO `tbl_interview` (`idinterview`, `idpelamar`, `p1`, `p2`, `p3`, `p4`, `p5`, `p6`, `p7`, `p8`, `p9`, `p10`, `p11`, `waktudata`) VALUES
(1, '202109.P-001', 'Keinginan yang kuat', 'Perusahaan yang bergerak di bidang industri', 'UMR', 'seminggu setelah diterima kerja', 'Ya', 'Ya', 'Bila dibawah tekanan', 'Tidak ada', 'Tidak ada', 'Ya', 'Pernah, organisasi keagamaan saat kuliah', '2021-09-08 10:25:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pelamar`
--

CREATE TABLE `tbl_pelamar` (
  `idpelamar` char(12) NOT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `agama` varchar(8) DEFAULT NULL,
  `stts_nikah` enum('1','2') DEFAULT NULL,
  `pendidikan_terakhir` varchar(3) DEFAULT NULL,
  `golongan_darah` varchar(2) DEFAULT NULL,
  `alamat_email` varchar(100) DEFAULT NULL,
  `noktp` char(16) DEFAULT NULL,
  `nohp` varchar(15) DEFAULT NULL,
  `asal` varchar(30) DEFAULT NULL,
  `riwayat_kerja` enum('Ada','Tidak') DEFAULT NULL,
  `idposisi` int(11) DEFAULT NULL,
  `status_pelamar` enum('Terima','Tolak') DEFAULT NULL,
  `prosesi` enum('Diproses','Selesai') NOT NULL,
  `waktudata` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pelamar`
--

INSERT INTO `tbl_pelamar` (`idpelamar`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jk`, `alamat`, `agama`, `stts_nikah`, `pendidikan_terakhir`, `golongan_darah`, `alamat_email`, `noktp`, `nohp`, `asal`, `riwayat_kerja`, `idposisi`, `status_pelamar`, `prosesi`, `waktudata`) VALUES
('202107.P-005', 'Wildan Nugraha 2', '372189', '1876-11-11', NULL, 'fsdjakl', NULL, NULL, NULL, NULL, NULL, 'j213789', NULL, 'fdsjakl', NULL, 1, 'Tolak', 'Diproses', '2021-07-14 12:28:32'),
('202107.P-006', 'Wildan Nugraha 2', '372189', '1876-11-11', NULL, 'fsdjakl', NULL, NULL, NULL, NULL, NULL, 'j213789', NULL, 'fdsjakl', NULL, 1, 'Tolak', 'Diproses', '2021-07-14 12:30:04'),
('202107.P-008', 'JKL', '123', '1978-03-12', NULL, 'fsjal', NULL, NULL, NULL, NULL, NULL, '57689', NULL, 'jklfs', NULL, 1, 'Tolak', 'Diproses', '2021-07-14 12:55:58'),
('202107.P-009', 'hg', 'fjskal', '1998-07-08', NULL, 'fjsdkla', NULL, NULL, NULL, NULL, NULL, '4832904798', NULL, 'fjsdkalj', NULL, 4, 'Tolak', 'Diproses', '2021-07-14 14:25:41'),
('202107.P-012', 'fsa', 'fdsa', '1988-07-04', NULL, 'dfsa', NULL, NULL, NULL, NULL, NULL, '43', '432', 'dfas', NULL, 1, 'Terima', 'Diproses', '2021-07-14 18:24:23'),
('202107.P-014', 'Wildan Nugraha', 'Jakarta', '1995-11-11', 'L', 'Tasikmalaya', 'Islam', '2', 's1', 'AB', 'wildan@mail.com', '327802xxxx', '085222xxx', 'Tasikmalaya', NULL, 1, 'Terima', 'Diproses', '2021-07-18 00:54:43'),
('202107.P-015', 'wea', 'fjdskal', '1997-05-05', NULL, 'djskal', NULL, NULL, NULL, NULL, NULL, '5342', '654', 'jfdksl', NULL, 2, 'Terima', 'Diproses', '2021-07-18 02:21:07'),
('202107.P-016', 'Wildan', 'Tasik', '1978-04-23', NULL, 'Bekasi', NULL, NULL, NULL, NULL, NULL, '327802xxxx', '085xxx', 'Bekasi', NULL, 1, 'Terima', 'Diproses', '2021-07-18 08:34:40'),
('202109.P-001', 'Wildan Nugraha', 'Jakarta', '2016-02-08', 'L', 'Tasikmalaya', 'Islam', '2', 's1', '-', 'as@m.c', '432432xxx', '543634', 'Tasik', 'Ada', 1, NULL, 'Diproses', '2021-09-08 10:25:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_persyaratan`
--

CREATE TABLE `tbl_persyaratan` (
  `idpersyaratan` int(11) NOT NULL,
  `idpelamar` char(12) DEFAULT NULL,
  `jenis` enum('Foto','KTP','KK','SKCK','Rekom','Jurulas') DEFAULT NULL,
  `lampiran` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_persyaratan`
--

INSERT INTO `tbl_persyaratan` (`idpersyaratan`, `idpelamar`, `jenis`, `lampiran`) VALUES
(2, '202107.P-008', 'KTP', '210714125558_file_KTP.jpg'),
(3, '202107.P-009', 'KTP', '210714022541_file_KTP.PNG'),
(13, '202107.P-012', 'KTP', '210714062423_file_KTP.PNG'),
(14, '202107.P-012', 'KK', '210714062423_file_KK.JPG'),
(15, '202107.P-012', 'SKCK', '210714062423_file_SKCK.PNG'),
(19, '202107.P-014', 'Foto', '210718125443_file_Foto.jpg'),
(20, '202107.P-014', 'KTP', '210718125443_file_KTP.jpg'),
(21, '202107.P-014', 'KK', '210718125443_file_KK.jpeg'),
(22, '202107.P-014', 'SKCK', '210718125443_file_SKCK.png'),
(23, '202107.P-015', 'Foto', '210718022108_file_Foto.PNG'),
(24, '202107.P-015', 'KTP', '210718022108_file_KTP.jpg'),
(25, '202107.P-015', 'KK', '210718022108_file_KK.jpeg'),
(26, '202107.P-015', 'SKCK', '210718022108_file_SKCK.png'),
(27, '202107.P-015', 'Rekom', '210718022108_file_Rekom.jpg'),
(28, '202107.P-016', 'Foto', '210718083441_file_Foto.jpg'),
(29, '202107.P-016', 'KTP', '210718083441_file_KTP.jpg'),
(30, '202107.P-016', 'KK', '210718083441_file_KK.PNG'),
(31, '202107.P-016', 'SKCK', '210718083441_file_SKCK.PNG'),
(40, '202109.P-001', 'Foto', '210908102554_file_Foto.PNG'),
(41, '202109.P-001', 'KTP', '210908102554_file_KTP.PNG'),
(42, '202109.P-001', 'KK', '210908102554_file_KK.jpg'),
(43, '202109.P-001', 'SKCK', '210908102554_file_SKCK.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_posisi`
--

CREATE TABLE `tbl_posisi` (
  `idposisi` int(11) NOT NULL,
  `nama_posisi` varchar(25) DEFAULT NULL,
  `value` varchar(20) NOT NULL,
  `deskripsi_posisi` text NOT NULL,
  `ketersediaan` enum('ada','tidak') DEFAULT NULL,
  `waktudata` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_posisi`
--

INSERT INTO `tbl_posisi` (`idposisi`, `nama_posisi`, `value`, `deskripsi_posisi`, `ketersediaan`, `waktudata`) VALUES
(1, 'Helper', 'helper', '', 'ada', '2021-09-06 10:17:29'),
(2, 'Carpenter (Tukang Kayu)', 'carpenter', '', 'ada', '2021-09-06 10:18:39'),
(3, 'Fitter', 'fitter', '', 'tidak', '2021-09-06 10:18:39'),
(4, 'Welder (Tukang Las)', 'welder', '', 'ada', '2021-09-06 10:18:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `iduser` char(12) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level` enum('admin','operator') DEFAULT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `statusdata` enum('aktif','tidak') DEFAULT NULL,
  `waktudata` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`iduser`, `username`, `password`, `level`, `nama`, `statusdata`, `waktudata`) VALUES
('1', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'Administrator', 'aktif', '2021-07-14 17:39:38');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`idconfig`);

--
-- Indeks untuk tabel `tbl_interview`
--
ALTER TABLE `tbl_interview`
  ADD PRIMARY KEY (`idinterview`);

--
-- Indeks untuk tabel `tbl_pelamar`
--
ALTER TABLE `tbl_pelamar`
  ADD PRIMARY KEY (`idpelamar`);

--
-- Indeks untuk tabel `tbl_persyaratan`
--
ALTER TABLE `tbl_persyaratan`
  ADD PRIMARY KEY (`idpersyaratan`);

--
-- Indeks untuk tabel `tbl_posisi`
--
ALTER TABLE `tbl_posisi`
  ADD PRIMARY KEY (`idposisi`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_config`
--
ALTER TABLE `tbl_config`
  MODIFY `idconfig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_interview`
--
ALTER TABLE `tbl_interview`
  MODIFY `idinterview` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_persyaratan`
--
ALTER TABLE `tbl_persyaratan`
  MODIFY `idpersyaratan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `tbl_posisi`
--
ALTER TABLE `tbl_posisi`
  MODIFY `idposisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
