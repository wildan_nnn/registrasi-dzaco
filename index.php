<?php
include "config/koneksi.php";
include "config/helper.php";
error_reporting(0);

$countpelamar = mysqli_fetch_assoc(mysqli_query($db, "SELECT count(*) as jml FROM tbl_pelamar "));
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Aplikasi registrasi online pt dzaco">
    <meta name="author" content="pt dzaco, anggi">
	<title>PT DZACO &mdash; Registrasi Calon Manpower</title>
	<link rel="icon" href="assets/img/cropped-Logo-kotak-192x192.png" />
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/blog.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/stepwizard.css" />
	<link rel="stylesheet" type="text/css" href="assets/fontawesome-free/css/all.min.css" />
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
	<style type="text/css">
		body {
			font-family: system-ui;
			background-image: url('assets/img/bekerja-cover.png');
			background-attachment: fixed;
			background-repeat: no-repeat;
			background-size: cover;
		}
		.navbar-nav a {
			font-weight: bold;
			text-transform: uppercase;
			font-family: system-ui;
		}
		.navbar-nav>li:hover {
			background-color: #A2A546;
			transition: 0.4s;
		}
		.navbar {
			border-bottom: 4px solid #A2A546;
		}
		
	</style>

	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/js/stepwizard.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- <a class="navbar-brand" href="#">Registrasi Pegawai</a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          	<ul class="nav navbar-nav">
            	<li class="<?php echo ($_GET[menu] == '' || $_GET[menu] == 'formpendaftar' ? 'active':'') ?>"><a href="?menu=formpendaftar">Form Pendaftaran</a></li>
            	<li><a href="?menu=manpower">Calon Manpower</a></li>
          	</ul>
          	<ul class="nav navbar-nav pull-right" id="main-navigation">
                <li><a href="#" class="pull-right">Jumlah Pelamar : <?=$countpelamar['jml']; ?> orang</a></li>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

	<div class="container">
		<section style="padding-top: 100px">
			<h4><marquee style="font-weight: bold;font-family: system-ui;">Hari ini tanggal <?= date('d').' '.bulan(date('m')).' '.date('Y') ?>. Terima kasih atas kunjungan Anda. Ini merupakan laman web PT Dzaco yang merupakan website resmi terkait rekruitmen PT Dzaco.</marquee></h4>
    		<?php
    		if(empty($_GET['menu']) || $_GET['menu'] == 'formpendaftar') { 
				include "konten/formpendaftar.php";
			} else if($_GET['menu'] == 'manpower') {
				include "konten/wait.php";
			} else if($_GET['menu'] == 'sudahdaftar') {
				include "konten/sudahdaftar.php";
			} 
			?>
		</section>
    </div>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#persy').hide();
			$('#interview').hide();
			$('#datatable').DataTable( {
		        "processing": true,
		        "serverSide": true,
		        "ajax": {
		        	url  : "konten/apipendaftar.php",
		        	type : 'POST',
		        	error: function(){  // error handling
                            $(".lookup-error").html("");
                            $("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                            $("#lookup_processing").css("display","none");
                            
                        }
		        },
		        "columns": [
	                  { "data": "no" },
	                  { "data": "nama" },
	                  { "data": "asal" },
	                  { "data": "posisi" },
	                  { "data": "status" },
	                  { "data": "waktu" },
	            ],  
		        language: {
			      'paginate': {
			        'previous' : "<span class='fa fa-angle-double-left'></span>",
			        'next' : "<span class='fa fa-angle-double-right'></span>",
			      }
			    },
		    });
		})
		
		$(".pos").on('change',function(){
			var posja = $('.pos').val();
			if(posja == '') {
				$('#persy').hide();
				$('#interview').hide();
				$('#rekom').show();
				$('#las').show();
			}
			else if(posja != ''){
				$('#persy').show();
				$('#interview').show();

				if(posja == 'helper') {
					$('#rekom').val('');
					$('#rekom').hide();
					$('#las').hide();
				}
				else if(posja == 'carpenter') {
					$('#rekom').show();
					$('#las').hide();
				}
				else if(posja == 'fitter') {
					$('#rekom').show();
					$('#las').hide();
				}
				else if(posja == 'welder') {
					$('#rekom').show();
					$('#las').show();
				}
			}

		})

	</script>
</body>
</html>