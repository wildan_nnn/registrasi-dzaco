<?php
	include "../config/koneksi.php";
	include "../config/helper.php";
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>PT DZACO &mdash; Cetak Manpower</title>
	<link rel="icon" href="../assets/img/cropped-Logo-kotak-192x192.png" />
	<style type="text/css">
		body {
			font-family:verdana;
		}
	</style>
</head>
<body>
	<table width="100%" border="0" align="center">
		<tr>
			<td width="40px" style="text-align: right"><img src="../assets/img/cropped-Logo-kotak-192x192.png" width="30px" /></td>
			<td style="font-weight: bold;color: #343C61">PT. DZACO</td>
		</tr>
		<tr>
			<td colspan="3"><hr /></td>
		</tr>
	</table>
	<div style="padding-bottom: 15pt">
		<table>
			<tr>
				<td width="30%">
					<img src="../assets/img/logo-april.png" width="100%" />	
				</td>
				<td style="font-weight: bold;text-align: center">
					CONTRACTOR ID BADGE APPLICATION FORM
				</td>
			</tr>
		</table>
	</div>
	<table width="100%" border="1" cellpadding="5" cellspacing="0" style="font-size: 9pt">
		<tr><td colspan="4">Employee Information</td></tr>
		<tr align="center">
			<td width="6px">No</td>
			<td width="170px">Stick 3x4 color photograph.<br/>
			Tempelkan pasfoto 3x4</td>
			<td>Detail</td>
			<td  width="190px">Stick copy of ID. <br />
			Tempelkan duplikat KTP
			</td>
		</tr>
		<?php
		$no = 0;
		$query = mysqli_query($db, "SELECT * FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE status_pelamar='Terima'");
		while ($show = mysqli_fetch_assoc($query)) { 
			$ktp = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM tbl_persyaratan WHERE jenis='KTP' AND idpelamar ='$show[idpelamar]'"));
			$foto = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM tbl_persyaratan WHERE jenis='Foto' AND idpelamar ='$show[idpelamar]'"));
			$no++;
		?>
		<tr>
			<td align="center"><?=$no ?></td>
			<td align="center"><img src="../assets/uploads/<?=$foto['lampiran'] ?>" width='120px'></td>
			<td valign='top'>
				<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tr>
						<td width="100px">Nama</td>
						<td width="5px">:</td>
						<td><?=$show['nama_lengkap'] ?></td>
					</tr>
					<tr>
						<td>Tempat, Tanggal lahir</td>
						<td>:</td>
						<td><?=$show['tempat_lahir'] ?>, <?=formattgl($show['tgl_lahir']) ?></td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td><?=$show['alamat'] ?></td>
					</tr>
					<tr>
						<td>No. KTP</td>
						<td>:</td>
						<td><?=$show['noktp'] ?></td>
					</tr>
					<tr>
						<td>Daerah Asal</td>
						<td>:</td>
						<td><?=$show['asal'] ?></td>
					</tr>
					<tr>
						<td>Posisi</td>
						<td>:</td>
						<td><?= $show['nama_posisi'] ?></td>
					</tr>
				</table>
			</td>
			<td align="center"><img src="../assets/uploads/<?=$ktp['lampiran'] ?>" width='160px' /></td>
		</tr>
		<?php
		}
		?>
			
	</table>
	<script type="text/javascript">
		window.print();
	</script>
</body>
</html>