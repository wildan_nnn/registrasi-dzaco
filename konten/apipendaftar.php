<?php
include "../config/koneksi.php";
$requestData = $_REQUEST;
$columns = array( 
// datatable column index  => database column name
    0 => 'idpelamar',
    1 => 'nama_lengkap', 
    2 => 'asal',
    3 => 'posisi',
    4 => 'waktudata',
    5 => 'status'  
);
//----------------------------------------------------------------------------------
//join 2 tabel dan bisa lebih, tergantung kebutuhan
$sql = "SELECT * FROM tbl_pelamar";
$query = mysqli_query($db, $sql);
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;

if( !empty($requestData['search']['value']) ) {
    // if there is a search parameter
    $sql  = "SELECT * FROM tbl_pelamar WHERE idpelamar LIKE '".$requestData['search']['value']."%' OR nama_lengkap LIKE '%".$requestData['search']['value']."%' "; 
    $sql .=" OR alamat LIKE '%".$requestData['search']['value']."%' ";
    $sql .=" OR asal LIKE '%".$requestData['search']['value']."%' ";
    $sql .=" OR posisi LIKE '".$requestData['search']['value']."%' ";
    $query=mysqli_query($db, $sql);
    $totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

    $sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query=mysqli_query($db, $sql); // again run query with limit
    
} else {    

    $sql = "SELECT * FROM tbl_pelamar ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($db, $sql);
    
}

$data = array();
while($show = mysqli_fetch_assoc($query))
{
	if($show['posisi'] == 'helper') {
		$pos = 'Helper';
	} elseif($show['posisi'] == 'carpenter') {
		$pos = 'Carpenter';
	} elseif($show['posisi'] == 'fitter') {
		$pos = 'Fitter';
	} elseif($show['posisi'] == 'weider') {
		$pos = 'Weider';
	}

	if(empty($show['status_pelamar'])){
		$stat = 'Diproses';
	} elseif($show['status_pelamar'] == 'Terima'){
		$stat = "<b style='color: green'>Diterima</b>";
	} elseif($show['status_pelamar'] == 'Tolak'){
		$stat = "<b style='color:red'>Tidak diterima</b>";
	} 

	$getar = array();
	$getar['no'] = $show['idpelamar'];
	$getar['nama'] = $show['nama_lengkap'];
	$getar['asal'] = $show['asal'];
	$getar['posisi'] = $pos;
	$getar['waktu'] = $show['waktudata'];
	$getar['status'] = $stat;
	
	$data[] = $getar;
}

$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ), 
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data );
echo json_encode($json_data);
