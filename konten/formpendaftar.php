<!-- <div class="jumbotron row" style="border-bottom-left-radius: 100px; ">
	<div class="col-md-8">
		<h1 style="font-family: system-ui;text-transform: uppercase;">Selamat Datang Para Pelamar!</h1>
		<p>
			Isi formulir dibawah untuk mendaftar!
		</p>
	</div>
	<div class="col-md-4">
		<img src="assets/img/pegawai-icon.png" width="110%" class="img-rounded">
	</div>
</div> -->

<?php
if($_GET['status'] == 'berhasil') { ?>
	<div class="alert alert-info alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  Berhasil disimpan
	</div>
<?php } 
	if($_GET['status'] == 'gagal') { ?>
	<div class="alert alert-danger alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  Gagal disimpan
	</div>
<?php } ?>

<div class="panel panel-success">
	<div class="panel-heading">Form Pendaftaran</div>
	<div class="panel-body">
		<div class="stepwizard">
		    <div class="stepwizard-row setup-panel">
		        <div class="stepwizard-step col-xs-3"> 
		            <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
		            <p><small>Panduan</small></p>
		        </div>
		        <div class="stepwizard-step col-xs-3"> 
		            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled>2</a>
		            <p><small>Isi Form</small></p>
		        </div>
		        <div class="stepwizard-step col-xs-3"> 
		            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
		            <p><small>Interview dan Kelengkapan Berkas</small></p>
		        </div>
		        <div class="stepwizard-step col-xs-3"> 
		            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
		            <p><small>Selesai</small></p>
		        </div>
		    </div>
		</div>

		<form action="" role="form" method="POST" enctype="multipart/form-data">
			<fieldset class="setup-content" id="step-1">
				<legend>Panduan pengisian form</legend>
				<ul>
					<li>Pengisian form lamaran disini menggunakan sistem aplikasi yang disetiap kolom diharuskan untuk diisi.</li>
					<li>Untuk persyaratan berkas, tolong dipersiapkan sesuai dengan posisi yang anda pilih nanti. </li>
					<li>Disaat anda memilih posisi yang akan dilamar, disitu langsung disediakan berkas mana saja yang harus anda persiapkan untuk di upload.</li>
					<li>Jika sudah cukup mengerti silakan klik Next.</li>
				</ul>
				<button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
			</fieldset>
			<fieldset class="setup-content" id="step-2">
			    <div class="form-group">
			      <label for="nama">Nama Lengkap</label>
			      <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Lengkap" required>
			    </div>
			    <div class="form-group row">
			    	<div class="col-md-12">
			      		<label for="ttl">Tempat Tanggal Lahir</label>
			    	</div>
			    	<div class="col-md-4">
			      		<input type="text" id="tempatl" name="tempatl" class="form-control" placeholder="Tempat Lahir" required>
			    	</div>
			    	<div class="col-md-4">
			      		<input type="date" id="tgll" name="tgll" class="form-control" placeholder="Tanggal lahir" required>
			    	</div>
			    </div>
			    <div class="form-group">
			      <label for="nama">Jenis Kelamin</label><br>
			      <input type="radio" name="jk" value="L" checked> Laki-laki&nbsp;
			      <input type="radio" name="jk" value="P"> Perempuan
			    </div>
			    <div class="form-group">
			      <label for="alm">Alamat</label>
			      <textarea name="alamat" class="form-control" required></textarea>
			    </div>
			    <div class="form-group row">
			    	<div class="col-md-6">
				      <label for="agm">Agama</label>
				      <select class="form-control" name="agama" required>
				      	<option value="">&mdash; Pilih &mdash;</option>
				      	<option value="Islam">Islam</option>
				      	<option value="Kristen">Kristen</option>
				      	<option value="Hindu">Hindu</option>
				      	<option value="Buddha">Buddha</option>
				      	<option value="Konghucu">Konghucu</option>
				      </select>
			    	</div>
			    	<div class="col-md-6">
			    		<label for="stsnikah">Status Pernikahan</label>
				      	<select class="form-control" name="nikah" required>
					      	<option value="">&mdash; Pilih &mdash;</option>
					      	<option value="1">Menikah</option>
					      	<option value="2">Belum Menikah</option>
				      	</select>
			    	</div>
			    </div>
			    <div class="form-group row">
			    	<div class="col-md-6">
				      	<label for="pendidikan">Pendidikan Terakhir</label>
				      	<select class="form-control" name="pendidikan" required>
					      	<option value="">&mdash; Pilih &mdash;</option>
					      	<option value="smp">SMP</option>
					      	<option value="sma">SMA</option>
					      	<option value="smk">SMK</option>
					      	<option value="d3">D3</option>
					      	<option value="s1">S1</option>
					    </select>
			    	</div>
			    	<div class="col-md-6">
			    		<label for="goldar">Golongan Darah</label>
				      	<select class="form-control" name="goldar" required>
					      	<option value="">&mdash; Pilih &mdash;</option>
					      	<option value="A">A</option>
					      	<option value="B">B</option>
					      	<option value="AB">AB</option>
					      	<option value="O">O</option>
					      	<option value="-">Lainnya</option>
					    </select>
			    	</div>
			    </div>
			    <div class="form-group">
			      <label for="mail">Email</label>
			      <input type="email" id="mail" name="mail" class="form-control" placeholder="Alamat Email" required>
			    </div>
			    <div class="form-group">
			      <label for="nama">No. KTP</label>
			      <input type="text" id="ktp" name="ktp" class="form-control" maxlength="16" placeholder="KTP" required>
			    </div>
			    <div class="form-group">
			      <label for="nama">No. HP</label>
			      <input type="text" id="nope" name="nope" class="form-control" placeholder="No. HP" required>
			    </div>
			    <div class="form-group">
			      <label for="nama">Daerah Asal</label>
			      <input type="text" id="asal" name="asal" class="form-control" placeholder="Asal Tempat" required>
			    </div>
			    <div class="form-group">
		    		<label for="goldar">Riwayat Pekerjaan</label>
			      	<select class="form-control" name="riwkerja" required>
			      		<option value="">&mdash; Pilih &mdash;</option>
				      	<option value="1">Ada</option>
				      	<option value="2">Tidak</option>
				    </select>
		    	</div>

			    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
			</fieldset>

			<fieldset class="setup-content" id="step-3">
			    <div class="form-group">
			      <label for="nama">Posisi yg Dilamar</label>
			      <select class="form-control pos" name="posisi" required>
			      	<option value="">-Pilih-</option>
			      	<?php
			      	$getPos = mysqli_query($db, "SELECT * FROM tbl_posisi WHERE ketersediaan='ada' ORDER BY idposisi ASC");
			      	while ($showPos = mysqli_fetch_array($getPos)) {
			      	?>
			      		<option value="<?=$showPos['value'] ?>"><?=$showPos['nama_posisi'] ?></option>
			      	<?php
			      	}
			      	?>
			      </select>
			    </div>

			  	<table class="table" id="persy">
			  		<thead>
			  			<tr bgcolor="grey"><th colspan="3" style="color: white">Persyaratan Berkas untuk Posisi yang dipilih</th></tr>
			  			<tr><td colspan="3" style="color: red;font-style: italic;font-size: 10pt">File Scan PNG, JPG atau JPEG maks ukuran file 2 MB! </td></tr>
				  		<tr>
				  			<th width="7px">No.</th>
				  			<th>Nama Berkas</th>
				  			<th class="col-md-4">Upload</th>
				  		</tr>
			  		</thead>
			  		<tbody>
			  			<tr>
				  			<td>1.</td>
				  			<td>Foto (Pasfoto 3x4)</td>
				  			<td><input type="file" name="file0" class="form-control" required></td>
				  		</tr>
			  			<tr>
				  			<td>2.</td>
				  			<td>KTP</td>
				  			<td><input type="file" name="file1" class="form-control" required></td>
				  		</tr>
				  		<tr>
				  			<td>3.</td>
				  			<td>KK</td>
				  			<td><input type="file" name="file2" class="form-control" required></td>
				  		</tr>
				  		<tr>
				  			<td>4.</td>
				  			<td>SKCK</td>
				  			<td><input type="file" name="file3" class="form-control" required></td>
				  		</tr>
				  		<tr id="rekom">
				  			<td>5.</td>
				  			<td>Surat Rekomendasi</td>
				  			<td><input type="file" name="file4" class="form-control"></td>
				  		</tr>
				  		<tr id="las">
				  			<td>6.</td>
				  			<td>Sertifikat Juru Las</td>
				  			<td><input type="file" name="file5" class="form-control"></td>
				  		</tr>
			  		</tbody>

			  	</table>

			  	<hr />
			  	<div id="interview">
				  	<h3>Minat dan Konsep Pribadi</h3>
				  	<div class="form-group">
				  		<label>1. Mengapa Anda ingin bekerja di Perusahaan kami?</label>
				  		<input type="text" class="form-control" name="p1" placeholder="Jawab..." required>
				  	</div>

				  	<div class="form-group">
				  		<label>2. Apa yang Anda ketahui mengenai Perusahaan kami?</label>
				  		<input type="text" class="form-control" name="p2" placeholder="Jawab..." required>
				  	</div>
				  	<div class="form-group">
				  		<label>3. Berapa gaji minimal yang Anda inginkan?</label>
				  		<input type="text" class="form-control" name="p3" placeholder="Jawab..." required>
				  	</div>
				  	<div class="form-group">
				  		<label>4. Kapan Anda mulai dapat bekerja?</label>
				  		<input type="text" class="form-control" name="p4" placeholder="Jawab..." required>
				  	</div>
				  	<div class="form-group">
				  		<label>5. Jika dibutuhkan Perusahaan, apakah Anda bersedia lembur?</label>
				  		<input type="text" class="form-control" name="p5" placeholder="Jawab..." required>
				  	</div>
				  	<div class="form-group">
				  		<label>6. Jika dibutuhkan Perusahaan, apakah Anda bersedia tugas lapangan / perjalanan dinas ke luar kota?</label>
				  		<input type="text" class="form-control" name="p6" placeholder="Jawab..." required>
				  	</div>
				  	
				  	<div class="form-group">
				  		<label>7. Terhadap hal-hal apakah Anda sulit mengambil keputusan?</label>
				  		<input type="text" class="form-control" name="p7" placeholder="Jawab..." required>
				  	</div>

				  	<hr />
				  	<h3>Aktifitas Sosial dan Kegiatan Lain</h3>
				  	<div class="form-group">
				  		<label>1. Apakah ada kenalan Anda di Perusahaan kami?</label>
				  		<input type="text" class="form-control" name="p8" placeholder="Jawab..." required>
				  	</div>
				  	<div class="form-group">
				  		<label>2. Jika ada sebutkan namanya!</label>
				  		<input type="text" class="form-control" name="p9" placeholder="Jawab..." required>
				  	</div>
				  	<div class="form-group">
				  		<label>3. Apakah Anda memiliki kendaraan pribadi?</label>
				  		<input type="text" class="form-control" name="p10" placeholder="Jawab..." required>
				  	</div>
				  	<div class="form-group">
				  		<label>4. Apakah Anda pernah memiliki mengikuti organisasi? Bila ada sebutkan apa saja!</label>
				  		<input type="text" class="form-control" name="p11" placeholder="Jawab..." required>
				  	</div>
			  	</div>

			  	<button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
			</fieldset>

			<fieldset class="setup-content" id="step-4">
			  	<legend>Finishing</legend>
			  	<h3>Apa anda sudah yakin dengan data yang anda isi?</h3>
			  	<ul>
				  	<li>Bila sudah yakin, silakan klik tombol <strong>Finish</strong>.</li> 
				  	<li>Jika masih ada data yang harus diubah silakan klik tombol step diatas mulai dari 1 - 4 untuk kembali.</li>
			  	</ul>
			  	<button type="submit" class="btn btn-danger pull-right btn-lg" name="submit" value="submit" onclick="return confirm('Dengan ini, data yang anda masukan sudah lengkap! Silakan klik OK!')">FINISH...</button>
			</fieldset>
		</form>
	</div>
</div>

<?php
if($_POST['submit']) {
	$nama = $_POST['nama'];
	$tempat = $_POST['tempatl'];
	$tgl = $_POST['tgll'];
	$jk = $_POST['jk'];
	$alamat = $_POST['alamat'];
	$agama = $_POST['agama'];
	$nikah = $_POST['nikah'];
	$pendidikan = $_POST['pendidikan'];
	$goldar = $_POST['goldar'];
	$mail = $_POST['mail'];
	$ktp = $_POST['ktp'];
	$nope = $_POST['nope'];
	$asal = $_POST['asal'];
	$riwker = $_POST['riwkerja'];
	$posisi = $_POST['posisi'];
	$time = date('Y-m-d H:i:s');

	$kode = trigger_pelamar();

	$cekposisi = mysqli_fetch_array(mysqli_query($db, "SELECT * FROM tbl_posisi WHERE value='$posisi'"));

	$simpan = mysqli_query($db, "INSERT INTO tbl_pelamar VALUES ('$kode','$nama','$tempat','$tgl','$jk','$alamat','$agama','$nikah','$pendidikan','$goldar','$mail','$ktp','$nope','$asal','$riwker','$cekposisi[idposisi]',NULL,'Diproses','$time')");

	$simpanInterview = mysqli_query($db, "INSERT INTO tbl_interview VALUES 
		(
			NULL,
			'$kode',
			'$_POST[p1]',
			'$_POST[p2]',
			'$_POST[p3]',
			'$_POST[p4]',
			'$_POST[p5]',
			'$_POST[p6]',
			'$_POST[p7]',
			'$_POST[p8]',
			'$_POST[p9]',
			'$_POST[p10]',
			'$_POST[p11]',
			'$time'
		)");
	
	if($simpan){
		if($posisi == 'helper') { // Dokumen Helper
			$no = 3;
		} elseif($posisi == 'carpenter') { // Dokumen Carpenter
			$no = 4;
		} elseif($posisi == 'fitter') { // Dokumen Fitter
			$no = 4;
		} elseif($posisi == 'welder') { // Dokumen Welder
			$no = 5;
		}
		for ($i=0; $i <= $no; $i++) {

			if(!empty($_FILES['file'.$i]['name'])){
		        // UPLOAD FILE DISINI
		        $file_name    = $_FILES['file'.$i]['name'];
		        $file_size    = $_FILES['file'.$i]['size'];
		        $file_ext    = explode('.', $file_name);
		        $file_tmp   = $_FILES['file'.$i]['tmp_name'];

		        if($i == 0){
		        	$jenis = 'Foto';
		        } elseif($i == 1){
		        	$jenis = 'KTP';
		        } elseif($i == 2){
		        	$jenis = 'KK';
		        } elseif($i == 3){
		        	$jenis = 'SKCK';
		        } elseif($i == 4){
		        	$jenis = 'Rekom';
		        } elseif($i == 5){
		        	$jenis = 'Jurulas';
		        }

		        if($file_size < 2048000)
		        {
		          	$filename = date('ymdhis').'_file_'.$jenis.'.'.end($file_ext);
		          	$lokasi = 'assets/uploads/'.$filename;
		          	move_uploaded_file($file_tmp, $lokasi);
		          	mysqli_query($db, "INSERT INTO tbl_persyaratan (idpersyaratan,idpelamar,jenis,lampiran) VALUES (NULL,'$kode','$jenis','$filename')");
		        } else {
		        	mysqli_query($db, "DELETE FROM tbl_pelamar WHERE idpelamar='$kode'");
		        	mysqli_query($db, "DELETE FROM tbl_persyaratan WHERE idpelamar='$kode'");
					echo "<script>alert('Maaf ukuran file maksimal 2 MB');document.location.href='?menu=formpendaftar'</script>";        	
		        }
		    }
		}
		
		echo "<script>document.location.href='?menu=sudahdaftar'</script>";
	} else {
		echo "<script>document.location.href='?menu=formpendaftar&status=gagal'</script>";
	}
}
?>