<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li class="active">Manpower</li>
	</ul><!-- /.breadcrumb -->

</div>

<div class="page-content">
	
	<div class="row">
		<div class="col-xs-12">
			<?php
			if(empty($_GET['act'])){ ?>
			<h3 class="header smaller lighter blue">Data Terbaru Calon Manpower</h3>

			<div class="clearfix">
				<div class="pull-right tableTools-container"></div>
			</div>
			<div class="table-header">
				&nbsp;
				<div class="pull-right" style="padding-right: 10px"><a href="cetakmanpower.php" target="_blank" class="btn btn-white btn-bold btn-xs"><span class="fa fa-print"></span> Cetak</a></div>
			</div>

			<div>
				<table id="datatable" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>No. Registrasi</th>
							<th>Nama</th>
							<th>Asal</th>
							<th>Posisi yg Dilamar</th>
							<th class="col-md-2">Status (Diterima/Tidak diterima)</th>
							<th class="col-md-2">Waktu Daftar</th>
							<th width="14%">Tools</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<?php
					 
					if(isset($_GET['hapus'])){
						$del = mysqli_query($db, "DELETE FROM tbl_pelamar WHERE idpelamar='$_GET[idp]'");
						if($del) {
							$qu = mysqli_query($db, "SELECT * FROM tbl_persyaratan WHERE idpelamar='$_GET[idp]'");
							$del2 = mysqli_query($db, "DELETE FROM tbl_persyaratan WHERE idpelamar='$_GET[idp]'");
							while($cari = mysqli_fetch_assoc($qu)){
								unlink('../assets/uploads/'.$cari['lampiran']);
							}
							echo "<script>alert('data berhasil dihapus');document.location.href='?menu=manpower'</script>";
						} else {
							echo "<script>alert('data gagal dihapus');document.location.href='?menu=manpower'</script>";
						}
					}

					if(isset($_GET['terima'])){
						$getpel = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE tbl_pelamar.idpelamar='$_GET[ids]'"));
						ini_set( 'display_errors', 1 );   
      					error_reporting( E_ALL );   
						$from = 'dzaco@dzaco-recruitment.com';
						$to = $getpel['alamat_email'];
						$subject = 'Pemberitahuan Status Lamaran';
						$message = "Selamat anda diterima diperusahaan kami dengan posisi ".$getpel['nama_posisi'];
						$headers = "From : ".$from;
						if(!empty($to)) {
							mail($to, $subject, $message, $headers);
						}

						$ter = mysqli_query($db, "UPDATE tbl_pelamar SET status_pelamar='Terima' WHERE idpelamar='$_GET[ids]'");
						if($ter) {
							echo "<script>document.location.href='?menu=manpower'</script>";
						} else {
							echo "<script>alert('data gagal dihapus');document.location.href='?menu=manpower'</script>";
						}
					}
					if(isset($_GET['tolak'])){
						$getpel = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE tbl_pelamar.idpelamar='$_GET[ids]'"));
						ini_set( 'display_errors', 1 );   
      					error_reporting( E_ALL );   
						$from = 'dzaco@dzaco-recruitment.com';
						$to = $getpel['alamat_email'];
						$subject = 'Pemberitahuan Status Lamaran';
						$message = "Mohon maaf, lamaran anda untuk posisi ".$getpel['nama_posisi']." kami TOLAK. Dikarenakan anda tidak memenuhi kualifikasi yang sudah kami terapkan disini. Silakan coba lagi di kesempatan selanjutnya.

							Terima Kasih.";
						$headers = "From : ".$from;
						if(!empty($to)) {
							mail($to, $subject, $message, $headers);
						}

						$ter = mysqli_query($db, "UPDATE tbl_pelamar SET status_pelamar='Tolak' WHERE idpelamar='$_GET[ids]'");
						if($ter) {
							echo "<script>document.location.href='?menu=manpower'</script>";
						} else {
							echo "<script>alert('data gagal dihapus');document.location.href='?menu=manpower'</script>";
						}
					}
					?>
				</table>
			</div>
			<?php 
			} elseif($_GET['act'] == 'detail') {
				include "detail-manpower.php";
			} elseif($_GET['act'] == 'hasilinterview') {
				include "resinterview.php";
			} 
			?>
		</div>		
	</div>
</div>