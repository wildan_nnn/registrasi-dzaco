<?php
$countpelamar = mysqli_fetch_assoc(mysqli_query($db, "SELECT count(*) as jml FROM tbl_pelamar"));
$jmlhelp = mysqli_fetch_assoc(mysqli_query($db, "SELECT count(*) as jmlhelp FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE value='helper'"));
$jmlcarp = mysqli_fetch_assoc(mysqli_query($db, "SELECT count(*) as jmlcarp FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE value='carpenter'"));
$jmlfit = mysqli_fetch_assoc(mysqli_query($db, "SELECT count(*) as jmlfit FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE value='fitter'"));
$jmlwei = mysqli_fetch_assoc(mysqli_query($db, "SELECT count(*) as jmlwei FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE value='welder'"));
?>
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li class="active">Dashboard</li>
	</ul><!-- /.breadcrumb -->

</div>

<div class="page-content">
	<div class="page-header">
		<h1>
			Dashboard
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
				overview &amp; stats
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-block alert-success">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>

				Selamat datang di laman akses
			</div>

			<div class="row">
				<div class="space-6"></div>

				<div class="col-sm-12 infobox-container">
					<div class="infobox infobox-pink">
						<div class="infobox-icon">
							<i class="ace-icon fa fa-users"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number"><?=$countpelamar['jml'] ?></span>
							<div class="infobox-content">calon manpower</div>
						</div>

						<!-- <div class="stat stat-success">8%</div> -->
					</div>

					<div class="infobox infobox-blue">
						<div class="infobox-icon">
							<i class="ace-icon fa fa-user"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number"><?=$jmlhelp['jmlhelp'] ?></span>
							<div class="infobox-content">posisi helper</div>
						</div>

					</div>

					<div class="infobox infobox-green">
						<div class="infobox-icon">
							<i class="ace-icon fa fa-tree"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number"><?=$jmlcarp['jmlcarp'] ?></span>
							<div class="infobox-content">posisi carpenter</div>
						</div>
					</div>

					<div class="infobox infobox-red">
						<div class="infobox-icon">
							<i class="ace-icon fa fa-wrench"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number"><?=$jmlfit['jmlfit'] ?></span>
							<div class="infobox-content">posisi fitter</div>
						</div>
					</div>

					<div class="infobox infobox-orange">
						<div class="infobox-icon">
							<i class="ace-icon fa fa-cog"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number"><?=$jmlwei['jmlwei'] ?></span>
							<div class="infobox-content">posisi welder</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>