<?php
include "../config/koneksi.php";
$requestData = $_REQUEST;
$columns = array( 
// datatable column index  => database column name
    0 => 'idpelamar',
    1 => 'nama_lengkap', 
    2 => 'asal',
    3 => 'posisi',
    4 => 'waktudata',
    5 => 'status'  
);
//----------------------------------------------------------------------------------
//join 2 tabel dan bisa lebih, tergantung kebutuhan
$sql = "SELECT * FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi";
$query = mysqli_query($db, $sql);
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;

if( !empty($requestData['search']['value']) ) {
    // if there is a search parameter
    $sql  = "SELECT * FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE idpelamar LIKE '".$requestData['search']['value']."%' OR nama_lengkap LIKE '%".$requestData['search']['value']."%' "; 
    $sql .=" OR alamat LIKE '%".$requestData['search']['value']."%' ";
    $sql .=" OR asal LIKE '%".$requestData['search']['value']."%' ";
    $sql .=" OR posisi LIKE '".$requestData['search']['value']."%' ";
    $query=mysqli_query($db, $sql);
    $totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

    $sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query=mysqli_query($db, $sql); // again run query with limit
    
} else {    

    $sql = "SELECT * FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($db, $sql);
    
}

$data = array();
while($show = mysqli_fetch_assoc($query))
{
	$pos = $show['nama_posisi'];

	// if(!empty($show['status_pelamar'])){
	// 	$stat = '<label class="pull-right inline">
	// 				<input id="id-button-borders" '.($show['status_pelamar'] == "Terima" ? "checked":"").' onchange=\'statusch("'.$show['idpelamar'].'","'.($show['status_pelamar'] == "Terima" ? "Tolak":"Terima").'")\' type="checkbox" class="ace ace-switch ace-switch-5">
	// 				<span class="lbl middle"></span>
	// 			</label>';
	// }
	if(empty($show['status_pelamar'])){
		$stat = "<a href='?menu=manpower&terima=1&ids=$show[idpelamar]' class='btn btn-success btn-minier'>Terima</a> <a href='?menu=manpower&tolak=1&ids=$show[idpelamar]' class='btn btn-warning btn-minier'>Tolak</a>";
	}
	elseif($show['status_pelamar'] == 'Terima'){
		$stat = "<span class='label label-sm label-success'>Diterima</span>";
	} elseif($show['status_pelamar'] == 'Tolak'){
		$stat = "<span class='label label-sm label-danger'>Tidak diterima</span>";
	} 

	$getar = array();
	$getar['no'] = $show['idpelamar'];
	$getar['nama'] = $show['nama_lengkap'];
	$getar['asal'] = $show['asal'];
	$getar['posisi'] = $pos;
	$getar['waktu'] = $show['waktudata'];
	$getar['status'] = '<center>'.$stat.'</center>';
	$getar['tools'] = "<div class='hidden-sm hidden-xs action-buttons'>
						<a class='blue' href='?menu=manpower&act=detail&ids=$show[idpelamar]' aria-label='Lihat Data' data-balloon-pos='up'>
							<i class='ace-icon fa fa-search-plus bigger-130'></i>
						</a>

						<a class='red' href='?menu=manpower&hapus=1&idp=$show[idpelamar]' aria-label='Hapus Data' data-balloon-pos='up' onclick=\"return confirm('Anda yakin akan menghapus data ini?')\">
										<i class='ace-icon fa fa-trash-o bigger-130'></i>
									</a>

						<a class='green' href='?menu=manpower&act=hasilinterview&idp=$show[idpelamar]' aria-label='Hasil Interview' data-balloon-pos='up'>
										<i class='ace-icon fa fa-clipboard bigger-130'></i>
									</a>
						</div>";
	
	$data[] = $getar;
}

$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ), 
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data );
echo json_encode($json_data);
