<?php
// Cek PIN
$cek = mysqli_fetch_array(mysqli_query($db, "SELECT * FROM tbl_config"));
?>
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
    <i class="ace-icon fa fa-cogs"></i>
    <li class="active">Konfigurasi</li>
  </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
  <div class="page-header">
    <h1>
      Pengaturan PIN
    </h1>
  </div><!-- /.page-header -->
  <div class="row">
    <div class="col-xs-12">
      <form class="form-validasi" id="fpin" name="form" action="#" method="POST" >
        <input type="hidden" name="idpin" id="idpin" value="<?=$cek['idconfig'] ?>">
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">PIN saat ini</label>
          <div class="col-sm-6">
            <b><?=$cek['pin'] ?></b>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Ganti PIN (6 digit)</label>
          <div class="col-sm-3">
            <input type="number" class="form-control" name="gantipin" id="gantipin" maxlength="6" placeholder="######">
          </div>
        </div>

        <button class="btn btn-info">Simpan</button>
      </form>
    </div>
  </div>
</div>

<div class="page-content">
  <div class="page-header">
    <h1>
      Pengaturan Posisi Yang Dilamar
    </h1>
  </div><!-- /.page-header -->
  <div class="row">
    <div class="col-xs-8">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Posisi</th>
            <!-- <th>Deskripsi</th> -->
            <th>Ketersediaan</th>
            <th>Tools</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          $getPosisi = mysqli_query($db, "SELECT * FROM tbl_posisi ORDER BY idposisi ASC");
          while ($showPosisi = mysqli_fetch_array($getPosisi)) {
            $no++;
          ?>  
          <tr>
              <td><?=$no ?></td>
              <td><?=$showPosisi['nama_posisi'] ?></td>
              <!-- <td><?=$showPosisi['deskripsi_posisi'] ?></td> -->
              <td>
                <center>

                <?php
                if($showPosisi['ketersediaan'] == 'ada'){
                  $stat = 'success';
                } else {
                  $stat = 'danger';
                }
                ?>
                <span class="badge badge-<?=$stat ?>"><?=strtoupper($showPosisi['ketersediaan']) ?></span>
                </center>
              </td>
              <td>
                <center>
                <label class="inline">
                  <input id="id-button-borders" <?= ($showPosisi['ketersediaan'] == "ada" ? "checked":"") ?> onchange="statussedia('<?=$showPosisi['idposisi'] ?>','<?=($showPosisi['ketersediaan'] == "ada" ? "tidak":"ada") ?>')" type="checkbox" class="ace ace-switch ace-switch-5">
                  <span class="lbl middle"></span>
                </label>
                </center>
              </td>
          </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500
      });
      toastr.options = {
        "timeOut": "3000",
      }
    $("#fpin").on('submit', function(e) {
      e.preventDefault()
      var vidpin = $("#idpin").val();
      var gantipin = $("#gantipin").val();
      if(gantipin.length > 6){
        toastr.error('Maaf PIN tidak boleh lebih dari 6 digit!')
      } else if(gantipin.length < 6){
        toastr.error('PIN harus 6 digit!')
      } else {
        $.ajax({
          url: '../konten/apikonfig.php',
          method: 'POST',
          data: { idpin: vidpin, pin: gantipin },
          success: function(response) {
            if(response == '1') {
                Toast.fire({
                    icon: 'success',
                    title: 'Password berhasil diubah!'
                })
                  .then(() => {
                      window.location = 'index.php?menu=konfig';
                });
            } else {
              toastr.error('Maaf ada kesalahan!')
            }
          }
        })
      }
    })
  });
</script>
