<?php
$query = mysqli_query($db, "SELECT * FROM tbl_pelamar INNER JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi WHERE idpelamar='$_GET[ids]'");
$getd = mysqli_fetch_assoc($query);
$foto = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM tbl_persyaratan WHERE jenis='Foto' AND idpelamar ='$getd[idpelamar]'"));
?>
<a href="?menu=manpower" class="btn btn-info btn-xs" style="margin: 6px;"><span class="fa fa-arrow-left"></span> Kembali</a>
<table class="table">
	<tr align="center"><td><img src="../assets/uploads/<?=$foto['lampiran'] ?>" width='150px'></td></tr>
	<tr>
		<table class="table">
			<tr>
				<td width="25%">&nbsp;</td>
				<td width="15%">Nama</td>
				<td width="5px">:</td>
				<td><?=$getd['nama_lengkap'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Tempat, Tanggal lahir</td>
				<td>:</td>
				<td><?=$getd['tempat_lahir'] ?>, <?=formattgl($getd['tgl_lahir']) ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Jenis Kelamin</td>
				<td>:</td>
				<td><?= ($getd['jk'] == 'L' ? 'Pria':'Wanita') ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Alamat</td>
				<td>:</td>
				<td><?=$getd['alamat'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Agama</td>
				<td>:</td>
				<td><?=$getd['agama'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Status Pernikahan</td>
				<td>:</td>
				<td><?= ($getd['stts_nikah'] == 1 ? 'Menikah':'Belum Menikah') ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Pendidikan Terakhir</td>
				<td>:</td>
				<td><?= strtoupper($getd['pendidikan_terakhir']) ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Golongan Darah</td>
				<td>:</td>
				<td><?=$getd['golongan_darah'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Email</td>
				<td>:</td>
				<td><?=$getd['alamat_email'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>No. KTP</td>
				<td>:</td>
				<td><?=$getd['noktp'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Daerah Asal</td>
				<td>:</td>
				<td><?=$getd['asal'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Posisi</td>
				<td>:</td>
				<td><?= $getd['nama_posisi'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>No. HP</td>
				<td>:</td>
				<td><?= $getd['nohp'] ?></td>
			</tr>
		</table>
	</tr>
</table>
<div class="table-header">Persyaratan berkas yang harus ada untuk posisi <?=$getd['nama_posisi'] ?></div>
<table class="table table-bordered">
	<?php
	if($getd['value'] == 'helper'){
		$jenis = array('Foto','KTP','KK','SKCK');
	} elseif($getd['value'] == 'carpenter'){
		$jenis = array('Foto','KTP','KK','SKCK','Rekom');
	} elseif($getd['value'] == 'fitter'){
		$jenis = array('Foto','KTP','KK','SKCK','Rekom');
	} elseif($getd['value'] == 'welder'){
		$jenis = array('Foto','KTP','KK','SKCK','Rekom','Jurulas');
	}
	foreach ($jenis as $value) {

		$qj = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM tbl_persyaratan WHERE jenis='$value'"));
		$q1 = mysqli_query($db, "SELECT * FROM tbl_persyaratan WHERE jenis='$value' AND idpelamar='$getd[idpelamar]'");
		$getdata = mysqli_fetch_assoc($q1);
		if(mysqli_num_rows($q1) > 0){
			$cek = 'checked';
		} else {
			$cek = '';
		}
		if($qj['jenis'] == 'Rekom'){
			$je = 'Surat Rekomendasi';
		} elseif($qj['jenis'] == 'Jurulas'){
			$je = 'Sertifikat Juru Las';
		} else {
			$je = $qj['jenis'];
		}
		?>
		<tr>
			<td width="10px">
				<div class="checkbox">
					<label>
						<input name="form-field-checkbox" type="checkbox" class="ace" <?=$cek ?>>
						<span class="lbl"></span>
					</label>
				</div>
			</td>
			<td width="250px"><?=$je ?></td>
			<td class="ace-thumbnails clearfix">
				<a href="../assets/uploads/<?=$getdata['lampiran'] ?>" data-rel="colorbox">
					<img src="../assets/uploads/<?=$getdata['lampiran'] ?>" width="150" height="150">
				</a>
			</td>
		</tr>
	<?php
	}
	?>
</table>