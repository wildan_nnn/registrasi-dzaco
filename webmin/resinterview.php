<a href="?menu=manpower" class="btn btn-info btn-xs" style="margin: 6px;"><span class="fa fa-arrow-left"></span> Kembali</a>
<?php
$cek = mysqli_num_rows(mysqli_query($db, "SELECT * FROM tbl_interview WHERE idpelamar='$_GET[idp]'"));
if($cek > 0) { 
	$query = mysqli_query($db, "SELECT * FROM tbl_interview 
		LEFT JOIN tbl_pelamar ON tbl_interview.idpelamar=tbl_pelamar.idpelamar
		LEFT JOIN tbl_posisi ON tbl_posisi.idposisi=tbl_pelamar.idposisi 
		WHERE tbl_interview.idpelamar='$_GET[idp]'");

	$getd = mysqli_fetch_assoc($query);

?>
	<table class="table">
		<tr>
			<th width="15%">No. Registrasi</th>
			<th width="5px">:</th>
			<th><?=$getd['idpelamar'] ?></th>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td><?=$getd['nama_lengkap'] ?></td>
		</tr>
		<tr>
			<td>Tempat, Tanggal lahir</td>
			<td>:</td>
			<td><?=$getd['tempat_lahir'] ?>, <?=formattgl($getd['tgl_lahir']) ?></td>
		</tr>
		<tr>
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td><?= ($getd['jk'] == 'L' ? 'Pria':'Wanita') ?></td>
		</tr>
		<tr>
			<td>Email</td>
			<td>:</td>
			<td><?=$getd['alamat_email'] ?></td>
		</tr>
		<tr>
			<td>Posisi</td>
			<td>:</td>
			<td><?= $getd['nama_posisi'] ?></td>
		</tr>
	</table>
	<div class="table-header">Hasil Sesi Interview</div>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="50%">Pertanyaan</th>
				<th>Jawaban</th>
			</tr>
		</thead>
		<?php
		$qj = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM tbl_interview WHERE idpelamar='$getd[idpelamar]'"));
		?>
		<tbody>
			<tr>
				<th bgcolor="#F0FFF0" colspan="2">Minat dan Konsep Pribadi</th>
			</tr>
			<tr>
				<th>1. Mengapa Anda ingin bekerja di Perusahaan kami?</th>
				<td><?=$qj['p1'] ?></td>
			</tr>
			<tr>
				<th>2. Apa yang Anda ketahui mengenai Perusahaan kami?</th>
				<td><?=$qj['p2'] ?></td>
			</tr>
			<tr>
				<th>3. Berapa gaji minimal yang Anda inginkan?</th>
				<td><?=$qj['p3'] ?></td>
			</tr>
			<tr>
				<th>4. Kapan Anda mulai dapat bekerja?</th>
				<td><?=$qj['p4'] ?></td>
			</tr>
			<tr>
				<th>5. Jika dibutuhkan Perusahaan, apakah Anda bersedia lembur?</th>
				<td><?=$qj['p5'] ?></td>
			</tr>
			<tr>
				<th>6. Jika dibutuhkan Perusahaan, apakah Anda bersedia tugas lapangan / perjalanan dinas ke luar kota?</th>
				<td><?=$qj['p6'] ?></td>
			</tr>
			<tr>
				<th>7. Terhadap hal-hal apakah Anda sulit mengambil keputusan?</th>
				<td><?=$qj['p7'] ?></td>
			</tr>
			<tr>
				<th bgcolor="#F0FFF0" colspan="2">Aktifitas Sosial dan Kegiatan Lain</th>
			</tr>
			<tr>
				<th>1. Apakah ada kenalan Anda di Perusahaan kami?</th>
				<td><?=$qj['p8'] ?></td>
			</tr>
			<tr>
				<th>2. Jika ada sebutkan namanya!</th>
				<td><?=$qj['p9'] ?></td>
			</tr>
			<tr>
				<th>3. Apakah Anda memiliki kendaraan pribadi?</th>
				<td><?=$qj['p10'] ?></td>
			</tr>
			<tr>
				<th>4. Apakah Anda pernah memiliki mengikuti organisasi? Bila ada sebutkan apa saja!</th>
				<td><?=$qj['p11'] ?></td>
			</tr>
		</tbody>
	</table>
<?php
} else { ?>
	<hr />
	<div class="alert alert-block alert-danger">
		<button type="button" class="close" data-dismiss="alert">
			<i class="ace-icon fa fa-times"></i>
		</button>

		Maaf manpower dengan nomor registrasi <?=$_GET['idp'] ?>, tidak mengisi sesi interview!
	</div>	
	
<?php
}
?>