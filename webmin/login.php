<?php
	error_reporting(0);
	include_once "../config/koneksi.php";	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>PT DZACO &mdash; Login</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="icon" href="../assets/img/cropped-Logo-kotak-192x192.png" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="../assets/fontawesome-free/css/all.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="../assets/css/ace.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />
		<!-- Toastr -->
  		<link rel="stylesheet" href="../assets/plugins/toastr/toastr.min.css">

	</head>

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<img src="../assets/img/cropped-Logo-kotak-192x192.png" width="20%">
									<span class="red"></span>
									<span class="white" id="id-text2"></span>
								</h1>
								<h4 class="blue" id="id-company-text">&copy; PT DZACO</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">

								<div class="signup-box widget-box visible no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger">
												Login dengan akunmu
											</h4>

											<div class="space-6"></div>
											<?php
											if(isset($_GET['session'])){
												echo "<i style='color: red'>Anda harus login dulu</i>";
											}
											?>
											<form action="" method="POST" id="login">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" id="username" class="form-control" placeholder="Username" autofocus />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" id="password" class="form-control" placeholder="Password" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<label class="inline">
															<input type="checkbox" class="ace" />
															<span class="lbl"> Remember Me</span>
														</label>

														<button class="width-35 pull-right btn btn-sm btn-success">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div>

										<div class="toolbar center">
											<a href="https://dzaco.com/" class="back-to-login-link">
												<i class="ace-icon fa fa-arrow-left"></i>
												Kembali
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.signup-box -->
							</div><!-- /.position-relative -->

							<div class="navbar-fixed-top align-right">
								<br />
								&nbsp;
								<a id="btn-login-dark" href="#">Dark</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-blur" href="#">Blur</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-light" href="#">Light</a>
								&nbsp; &nbsp; &nbsp;
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="../assets/js/jquery.min.js"></script>
		<!-- Sweetalert -->
    	<script src="../assets/plugins/sweetalert2/sweetalert2.min.js"></script>
    	<!-- Toastr -->
  		<script src="../assets/plugins/toastr/toastr.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});
			
			
			
			//you don't need this, just used for changing background
			jQuery(function($) {
			 $('#btn-login-dark').on('click', function(e) {
				$('body').attr('class', 'login-layout');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'blue');
				
				e.preventDefault();
			 });
			 $('#btn-login-light').on('click', function(e) {
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');
				
				e.preventDefault();
			 });
			 $('#btn-login-blur').on('click', function(e) {
				$('body').attr('class', 'login-layout blur-login');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'light-blue');
				
				e.preventDefault();
			 });
			 
			});

	    	$(document).ready(function() {
	    		const Toast = Swal.mixin({
			      toast: true,
			      position: 'top-end',
			      showConfirmButton: false,
			      timer: 1500
			    });
			    toastr.options = {
			    	"timeOut": "3000",
			    }
	    		$("#login").on('submit',function(e){
	    			e.preventDefault()
			        var username = $("#username").val().trim();
			        var password = $("#password").val().trim();

			        if( username != "" && password != "" ){
			            $.ajax({
			                url:'plogin.php',
			                type:'post',
			                data:{user:username,pass:password},
			                success:function(response){
			                	// console.log(response)
			                    var msg = "";
			                    if(response == 1){
			                    	Toast.fire({
								        icon: 'success',
								        title: 'Selamat, anda berhasil login!'
								    })
	                                .then(() => {
	                                    window.location = 'index.php';
	                                });
			                    }else{
			                    	toastr.error('Maaf username dan/atau password tidak sesuai!')
			                    }
			                }
			            });
			        }
			    });
	    	});
	    </script>
	</body>
</html>
