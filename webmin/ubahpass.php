<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
    <li>
      <i class="ace-icon fa fa-home home-icon"></i>
      <a href="#">Home</a>
    </li>
    <li class="active">Ubah Password</li>
  </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
  <div class="page-header">
    <h1>
      Ubah Password
    </h1>
  </div><!-- /.page-header -->
  <div class="row">
    <div class="col-xs-12">
      <form class="form-validasi" id="form" name="form" action="#" method="POST" >
        <input type="hidden" id="user" value="<?=$_SESSION['user'] ?>">
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Password Lama</label>
          <div class="col-sm-6">
            <input type="password" name="passla" id="passlama" class="form-control" maxlength="50" required="">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Password Baru</label>
          <div class="col-sm-6">
            <input type="password" class="form-control" name="passba" id="passba" maxlength="30" required="">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Konfirmasi Password</label>
          <div class="col-sm-6">
            <input type="password" name="konpas" class="form-control " id="konpass" maxlength="30" required="">
          </div>
        </div>
        <button class="btn btn-info">Simpan</button>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(document).ready(function() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500
      });
      toastr.options = {
        "timeOut": "3000",
      }
    $("#form").on('submit', function(e) {
      e.preventDefault()
      var user = $("#user").val();
      var passla = $("#passlama").val().trim();
      var passba = $("#passba").val().trim();
      var konpas = $("#konpass").val().trim();
      $.ajax({
        url: '../konten/apicekuser.php',
        method: 'POST',
        data: { user: user, pass: passla },
        success: function(response) {
          if(response == '1') {
            if(passba == konpas) {
              $.ajax({
                url: '../konten/apiubahpass.php',
                method: 'POST',
                data: { user: user, pass: konpas },
                success: function(response) {
                  if(response == '1'){
                    Toast.fire({
                        icon: 'success',
                        title: 'Password berhasil diubah!'
                    })
                      .then(() => {
                          window.location = 'index.php?menu=ubahpass';
                      });
                  } 
                }
              });
            } else {
              toastr.error('Maaf password baru dan konfirmasi password harus sama!')
            }
          } else {
            toastr.error('Maaf password lama tidak cocok!')
          }
        }
      })
    })
  });
</script>
