<?php
session_start();
include "../config/koneksi.php";
include "../config/helper.php";
if(empty($_SESSION['user'])){
	header("location: login.php?session=1");
}
?>
<!DOCTYPE html>
<html lang="en">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>PT DZACO - Halaman <?=($_SESSION['level'] == 'admin' ? 'Administrator':'Operator') ?></title>

	<meta name="description" content="overview &amp; stats" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	<link rel="icon" href="../assets/img/cropped-Logo-kotak-192x192.png" />
	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" /> <!-- Version 5 -->

	<!-- page specific plugin styles -->
	<link rel="stylesheet" href="../assets/css/colorbox.min.css" />

	<!-- text fonts -->
	<link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />
	<link rel="stylesheet" href="../assets/css/balloon.min.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
	

	<!--[if lte IE 9]>
		<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
	<![endif]-->
	<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
	<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />


	<link rel="stylesheet" href="../assets/plugins/toastr/toastr.min.css">

	<script src="../assets/js/ace-extra.min.js"></script>

	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/jquery.colorbox.min.js"></script>

	<!-- ace scripts -->
	<script src="../assets/js/ace-elements.min.js"></script>
	<script src="../assets/js/ace.min.js"></script>
	<!-- Sweetalert -->
	<script src="../assets/plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Toastr -->
	<script src="../assets/plugins/toastr/toastr.min.js"></script>
	<script src="../assets/js/jquery.dataTables.min.js"></script>
	<script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
	
	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="index.html" class="navbar-brand">
						<small>
							<img src="../assets/img/cropped-Logo-kotak-192x192.png" width="8%">
							PT DZACO
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="../assets/img/avatars/avatar.png" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									<?=$_SESSION['nama'] ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

								<!-- <li>
									<a href="profile.html">
										<i class="ace-icon fa fa-user"></i>
										Profile
									</a>
								</li> -->

								<li>
									<a href="../" target="_blank">
										<i class="ace-icon fa fa-globe"></i>
										View Web
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="logout.php">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">

				<ul class="nav nav-list">
					<li class="<?= empty($_GET['menu']) ? 'active':'' ?>">
						<a href="index.php">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="<?= $_GET['menu'] == 'manpower' ? 'active':'' ?>">
						<a href="?menu=manpower">
							<i class="menu-icon fa fa-users"></i>
							<span class="menu-text">
								Manpower
							</span>

						</a>

						<b class="arrow"></b>
					</li>

					<!-- <li class="<?= $_GET['menu'] == 'hasilinterview' ? 'active':'' ?>">
						<a href="?menu=hasilinterview">
							<i class="menu-icon fa fa-clipboard"></i>
							<span class="menu-text">
								Hasil Interview
							</span>

						</a>

						<b class="arrow"></b>
					</li> -->

					<li class="<?= $_GET['menu'] == 'ubahpass' ? 'active':'' ?>">
						<a href="?menu=ubahpass">
							<i class="menu-icon fa fa-unlock"></i>
							<span class="menu-text">
								Ubah password
							</span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="<?= $_GET['menu'] == 'konfig' ? 'active':'' ?>">
						<a href="?menu=konfig">
							<i class="menu-icon fa fa-cogs"></i>
							<span class="menu-text">
								Konfigurasi
							</span>
						</a>

						<b class="arrow"></b>
					</li>

				</ul>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<?php
					if(empty($_GET['menu'])){
						include "dasbor.php";
					} elseif($_GET['menu'] == 'manpower') {
						include "manpower.php";
					} elseif($_GET['menu'] == 'ubahpass') {
						include "ubahpass.php";
					} elseif($_GET['menu'] == 'konfig') {
						include "konfig.php";
					} 
					?>
				</div>

			</div>

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">PT DZACO</span>
							&copy; 2021
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

						</span>
					</div>
				</div>
			</div>
		</div>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			$(function($) {

				var $overflow = '';
				var colorbox_params = {
					rel: 'colorbox',
					reposition:true,
					scalePhotos:true,
					scrolling:false,
					previous:'<i class="ace-icon fa fa-arrow-left"></i>',
					next:'<i class="ace-icon fa fa-arrow-right"></i>',
					close:'&times;',
					current:'{current} of {total}',
					maxWidth:'100%',
					maxHeight:'100%',
					onOpen:function(){
						$overflow = document.body.style.overflow;
						document.body.style.overflow = 'hidden';
					},
					onClosed:function(){
						document.body.style.overflow = $overflow;
					},
					onComplete:function(){
						$.colorbox.resize();
					}
				};

				$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
				$("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon
				
				
				$(document).one('ajaxloadstart.page', function(e) {
					$('#colorbox, #cboxOverlay').remove();
			   });
			
			
			  //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
			  //but sometimes it brings up errors with normal resize event handlers
			  // $.resize.throttleWindow = false;
			
			
			
			  //pie chart tooltip example
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;
			
				/////////////////////////////////////
				$(document).one('ajaxloadstart.page', function(e) {
					$tooltip.remove();
				});
			
			
			
				$('#recent-box [data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('.tab-content')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					//var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			
			
				$('.dialogs,.comments').ace_scroll({
					size: 300
			    });
				
				
				//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
				//so disable dragging when clicking on label
				var agent = navigator.userAgent.toLowerCase();
				if(ace.vars['touch'] && ace.vars['android']) {
				  $('#tasks').on('touchstart', function(e){
					var li = $(e.target).closest('#tasks li');
					if(li.length == 0)return;
					var label = li.find('label.inline').get(0);
					if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
				  });
				}
			
				
				
			
			
				//show the dropdowns on top or bottom depending on window height and menu position
				$('#task-tab .dropdown-hover').on('mouseenter', function(e) {
					var offset = $(this).offset();
			
					var $w = $(window)
					if (offset.top > $w.scrollTop() + $w.innerHeight() - 100) 
						$(this).addClass('dropup');
					else $(this).removeClass('dropup');
				});

				$('#datatable').DataTable( {
					"processing": true,
		        	"serverSide": true,
		        	"ajax": {
			        	url  : "apimanpower.php",
			        	type : 'POST',
			        	error: function(){  // error handling
	                            $(".lookup-error").html("");
	                            $("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
	                            $("#lookup_processing").css("display","none");
	                            
	                        }
			        },
			         "columns": [
		                  { "data": "no" },
		                  { "data": "nama" },
		                  { "data": "asal" },
		                  { "data": "posisi" },
		                  { "data": "status" },
		                  { "data": "waktu" },
		                  { "data": "tools" },
		            ],  
			        language: {
				      'paginate': {
				        'previous' : "<span class='fa fa-angle-double-left'></span>",
				        'next' : "<span class='fa fa-angle-double-right'></span>",
				      }
				    }
			    });

			
			})

			function statusch(ids,status)
			{
			    $.ajax({
			      type: 'GET',
			      url: "apistatuspelamar.php?status=" + status + "&id=" + ids,
			      success: function(){
			        location.reload()
			      }
			    })
			}

			function statussedia(ids,status)
			{
			    $.ajax({
			      type: 'GET',
			      url: "apisediaposisi.php?status=" + status + "&id=" + ids,
			      success: function(){
			        location.reload()
			      }
			    })
			}
		</script>
	</body>
</html>