<?php
function bulan($bln) {
	$bulan = $bln;
	if($bulan == '01') {
		return 'Januari';
	} elseif($bulan == '02') {
		return 'Februari';
	} elseif($bulan == '03') {
		return 'Maret';
	} elseif($bulan == '04') {
		return 'April';
	} elseif($bulan == '05') {
		return 'Mei';
	} elseif($bulan == '06') {
		return 'Juni';
	} elseif($bulan == '07') {
		return 'Juli';
	} elseif($bulan == '08') {
		return 'Agustus';
	} elseif($bulan == '09') {
		return 'September';
	} elseif($bulan == '10') {
		return 'Oktober';
	} elseif($bulan == '11') {
		return 'Nopember';
	} elseif($bulan == '12') {
		return 'Desember';
	}
}

function formattgl($tgl) {
	$vtgl = explode('-', $tgl);
	return $vtgl[2].' '.bulan($vtgl[1]).' '.$vtgl[0];
}

function trigger_pelamar() {
	include "koneksi.php";
	$peng = mysqli_query($db, "select max(right(idpelamar,3)) as fo from tbl_pelamar where year(waktudata)=year(now()) and month(waktudata)=month(now())");
	$fetchkode = mysqli_fetch_assoc($peng);
	if ($fetchkode['fo'] == 0 OR $fetchkode['fo'] == NULL) {
		$getkode = mysqli_fetch_assoc(mysqli_query($db, "select concat(date_format(now(),'%Y%m.P-'),lpad(count(idpelamar)+1,3,0)) as kode from tbl_pelamar where year(waktudata)=year(now()) and month(waktudata)=month(now())"));
	} else {
		$getkode = mysqli_fetch_assoc(mysqli_query($db, "select concat(date_format(now(),'%Y%m.P-'),lpad(max(right(idpelamar,3))+1,3,0)) as kode from tbl_pelamar where year(waktudata)=year(now()) and month(waktudata)=month(now())"));
	}
	return $getkode['kode'];

}